#target illustrator

// jooSnapToDocumentGrid.jsx v1.0
//
// Copyright (c) 2016 Janne Ojala
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

main();

function main(){
    var grid = getDocumentGrid();
    snapSelectedPathToDocumentGrid(activeDocument.selection, grid); 
}

function snapSelectedPathToDocumentGrid(sel, grid){
    for(var i = 0; i < sel.length; i++){
        var pp = sel[i].pathPoints;
        for(var j = 0; j < pp.length; j++){
            var p = pp[j];
            if ( p.selected == PathPointSelection.NOSELECTION )
              continue;          
            else if ( p.selected == PathPointSelection.ANCHORPOINT || 
                      p.selected == PathPointSelection.LEFTRIGHTPOINT  ){
                p.leftDirection = nearestGrid(p.leftDirection, grid);
                p.rightDirection = nearestGrid(p.rightDirection, grid);
                p.anchor = nearestGrid(p.anchor, grid);
            }                          
        }
    }
}

function getDocumentGrid(){
    var prf = app.preferences;
    var ticks = prf.getIntegerPreference('Grid/Horizontal/Ticks');
    var spacing = prf.getRealPreference('Grid/Horizontal/Spacing');
    return spacing/ticks;    
}

function nearestGrid(anchor, grid) {
    return [Math.round(anchor[0] / grid) * grid, 
            Math.round(anchor[1] / grid) * grid ];
}