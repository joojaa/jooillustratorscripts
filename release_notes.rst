﻿Release Notes for  jooIllustratorScripts
========================================

5.4.2018
--------

jooGraphFunction.jsx ver 0.91b

    - Added ability to choose plot method
      currently supporting either smooth
      or linear

15.7.2016
---------

Added jooSnapToDocumentGrid 1.0

30.12.2015
----------

Started numbering versions so that it is easier to know
which release version you are using. 

jooGraphFunction.jsx is now at ver 0.9b
    
    - Estimator function has been improved
    - Dialog now remembers last used setting

jooHandlesToGeometry.jsx is now at ver 1.0