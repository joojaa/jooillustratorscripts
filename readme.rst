﻿jooIllustratorScripts
=====================

This is a collection of scripts that might be useful for
others. 


jooGraphFunction (see `Documentation <https://bitbucket.org/joojaa/jooillustratorscripts/wiki/Documentation/jooGraphFunction>`_)
------------------------

Sometimes it would be handy to graph a mathematical functions. This script uses
a simple technique to estimate the function as smooth beziers curves or as 
linear segments. 


.. figure:: img/jooGraphFunction.png

    Plot of parametric function :math:`\big(20\sin(t) + 23\cos(2t), 20\cos(t) + 23\sin(2t)\big)`.
    
See `documentation <https://bitbucket.org/joojaa/jooillustratorscripts/wiki/Documentation/jooGraphFunction>`_ 
for more examples of functions you can plot.

jooHandlesToGeometry
--------------------

This script converts the Bézier manipulator handles to geometry. This can be
useful if you need to publish tutorials, especially for print. It might also be 
used to customize the look and feel.

.. figure:: img/jooHandlesToGeometry.png

   Example of the result of running jooHandlesToGeometry on selection.

jooSnapToDocumentGrid
---------------------

This function snaps selected points to the nearest document grid point. 

.. figure:: img/jooSnapToDocumentGrid.png

   Quickly snap selected points to grid.