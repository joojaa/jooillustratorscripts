﻿#target illustrator

// jooGraphFunction.jsx  v0.91b
//
// Copyright (c) 2015-2018 Janne Ojala
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

try {
    app.activeDocument;
    var gui = buildGUI();
} catch(err) {
    alert("please open a document");    
}



function buildGUI(){
    var resource =
    "dialog { text:'jooGraphFunction',\
        properties:{ closeButton:true, maximizeButton:false,\
            minimizeButton:false, resizeable:false },\
        orientation:'column', spacing:2, margins:5,\
        alignChildren:['left','fill'],\
        g: Group {\
                alignChildren:['left','fill']\
                orientation:'row', spacing:2,\
            p1: Panel {\
                text: 'Parametric Function:',\
                g1: Group {\
                    orientation:'column', spacing:2,\
                    f1g: Group {\
                        st: StaticText { text:'x:'},\
                        f: EditText {text:'20 * sin(t) + 300', alignment:['fill','top'], characters:40}\
                    },\
                    f2g: Group {st2 : StaticText{ text:'y:'},\
                        f: EditText { text:'20 * cos(t) - 200', alignment:['fill','center'], characters:40 },\
                    },\
                },\
                opt: Group {alignment:['left','bottom'], orientation:'row', spacing:2, size: [272,20] \
                pvw: Checkbox {text:'Preview'},\
                dsc: StaticText { text:'\t\t\t\tMethod:',alignment:['right','center']},\
                mthd: DropDownList{ alignment:['right','center'],\
                  }\
                }\
            },\
            p2: Panel {text: 'Range:',\
                g2: Group {\
                    orientation:'column', spacing:2,\
                    gmin: Group {st : StaticText{ text:'min:'}\
                        min: EditText {text:'0', characters:10}},\
                    gmax: Group {st : StaticText{ text:'max:'}\
                        max: EditText { text:'PI', characters:10}},\
                    gstep: Group {st : StaticText{ text:'step:'}\
                        step: EditText { text:'PI/4', characters:10}},\
               },\
           }\
       }\
       g3: Group { alignment: 'right',\
           plot: Button { text: 'copy'},\
           ok: Button { text:'Ok'},\
       }\
    }"
    
    var fncs = [plotSmooth, plotLinear];  
    
    var setDefaultVals = function() {
        var item = app.preferences.getStringPreference('jooGraphFunction/last/x');  
        if (item === '')
           item = '20 * sin(t) + 300';
        win.g.p1.g1.f1g.f.text = item;
        
        var item = app.preferences.getStringPreference('jooGraphFunction/last/y');  
        if (item === '')
           item = '20 * cos(t) - 200';
        win.g.p1.g1.f2g.f.text = item;  
        
        var item = app.preferences.getStringPreference('jooGraphFunction/last/min');  
        if (item === '')
           item = '0';
        win.g.p2.g2.gmin.min.text = item;  
    
        var item = app.preferences.getStringPreference('jooGraphFunction/last/max');  
        if (item === '')
           item = 'PI';
        win.g.p2.g2.gmax.max.text = item;  
        
        var item = app.preferences.getStringPreference('jooGraphFunction/last/step');  
        if (item === '')
           item = 'PI/2';
        win.g.p2.g2.gstep.step.text = item;  
    }

    var saveDefaultVals = function() {
        app.preferences.removePreference('jooGraphFunction/last/x');
        app.preferences.removePreference('jooGraphFunction/last/y');
        app.preferences.removePreference('jooGraphFunction/last/min');
        app.preferences.removePreference('jooGraphFunction/last/max');
        app.preferences.removePreference('jooGraphFunction/last/step');

        app.preferences.setStringPreference('jooGraphFunction/last/x',
                                             win.g.p1.g1.f1g.f.text);  
        app.preferences.setStringPreference('jooGraphFunction/last/y',
                                             win.g.p1.g1.f2g.f.text);
        app.preferences.setStringPreference('jooGraphFunction/last/min',
                                             win.g.p2.g2.gmin.min.text);
        app.preferences.setStringPreference('jooGraphFunction/last/max',
                                             win.g.p2.g2.gmax.max.text);
        app.preferences.setStringPreference('jooGraphFunction/last/step',
                                             win.g.p2.g2.gstep.step.text);
    }    
    
    var win = new Window (resource);
    setDefaultVals();    
    
    var abs = Math.abs;
    var acos = Math.acos;
    var acosh = Math.acosh;
    var asin = Math.asin;
    var asinh = Math.asinh;
    var atan = Math.atan;
    var atan2 = Math.atan2;
    var atanh = Math.atanh;
    var cbrt = Math.cbrt;
    var ceil = Math.ceil;
    var cos = Math.cos;
    var cosh = Math.cosh;
    var exp = Math.exp;
    var floor = Math.floor;
    var fround = Math.fround;
    var hypot = Math.hypot;
    var imul = Math.imul;
    var log = Math.log;
    var log1 = Math.log1;
    var log1p = Math.log1p;
    var log2 = Math.log2;
    var max = Math.max;
    var min = Math.min;
    var pow = Math.pow;
    var random = Math.random;
    var round = Math.round;
    var sign = Math.sign;
    var sin = Math.sin;
    var sinh = Math.sinh;
    var sqrt = Math.sqrt;
    var tan = Math.tan;
    var tanh = Math.tanh;
    var trunc = Math.trunc;
    var E = Math.E;
    var LN10 = Math.LN10;
    var LN2 = Math.LN2;
    var LOG10E = Math.LOG10E;
    var LOG2E = Math.LOG2E;
    var PI = Math.PI;
    var SQRT1_2 = Math.SQRT1_2;
    var SQRT2 = Math.SQRT2;

    
    var alerterror = function(item) {
        item.graphics.backgroundColor = item.graphics.newBrush (item.graphics.BrushType.SOLID_COLOR, [1.0, 0.5, 0.5]);  
    }

    var clearalert = function(item) {
        item.graphics.backgroundColor = item.graphics.newBrush (item.graphics.BrushType.SOLID_COLOR, [1.0, 1.0, 1.0]);  
    }

    var buildParamFunc = function(item) {
        try {
            var test = new Function('t', "return "+ item.text);
            test(0);
            clearalert(item);
        } catch (err) { 
           alerterror(item);
           return null;
        }
        return test;
    }
    
    var buildFunc = function() {
        var x = buildParamFunc(win.g.p1.g1.f1g.f);
        var y = buildParamFunc(win.g.p1.g1.f2g.f);
        
        //if (x===null || y===null) return null;
        return  function(t) {return new Array(x(t),y(t))};
    } 
    
    var plot = function() {
         if( previewObj ) return;
         var func = buildFunc();
         var f = fncs[win.g.p1.opt.mthd.selection.index];
         f( eval(win.g.p2.g2.gmin.min.text), 
            eval(win.g.p2.g2.gstep.step.text),
            eval(win.g.p2.g2.gmax.max.text), 
            0.01, func);
         redraw();
    }
    var previewObj = null;
 
    var preview = function() {

        var func = buildFunc();
       
        try {
            var f = fncs[win.g.p1.opt.mthd.selection.index];
            var temp = f( eval(win.g.p2.g2.gmin.min.text), 
                        eval(win.g.p2.g2.gstep.step.text),
                        eval(win.g.p2.g2.gmax.max.text), 
                        0.01, func);
        } catch (err) { 
            return;
        }
        if(previewObj) previewObj.remove();
        previewObj = temp;
        redraw();
    }


    var setUpPreview = function() {
        win.g.p1.g1.f1g.f.addEventListener('change', preview, false );
        win.g.p1.g1.f2g.f.addEventListener('change', preview, false );
        win.g.p2.g2.gmin.min.addEventListener('change', preview, false ); 
        win.g.p2.g2.gstep.step.addEventListener('change', preview, false ); 
        win.g.p2.g2.gmax.max.addEventListener('change', preview, false );
        preview();
    }

    var ok = function() {
         if (win.g.p1.opt.pvw.value){
             win.close(0);
         } else {
             try {
                 plot();
             } catch (err) { 
                return;
             }
         }
         saveDefaultVals();
         win.close(0);
    }

    var s = win.g.p1.opt.mthd.add("item","Smooth");
    win.g.p1.opt.mthd.selection = s;
    win.g.p1.opt.mthd.add("item","Linear");
    win.g3.plot.onClick = plot;
    win.g3.ok.onClick = ok;
    win.g.p1.opt.pvw.onClick = setUpPreview;
    win.show();
    return win;
}


function plotLinear(start, step, end, delta,func){
    var doc = app.activeDocument;
    var path  = doc.pathItems.add();

    var index = 0;

    for (var t = start;  t <= end; t += step) {
        var point = path.pathPoints.add();
        var pos = func(t);
        point.anchor = pos;
        point.leftDirection = pos;
        point.rightDirection = pos;
    }
    return path;
}

function dist(a, b){
  return Math.sqrt(Math.pow(b[0]-a[0], 2) + 
                   Math.pow(b[1]-a[1], 2));        
}

// this is a  very dirty and naive estimation 
function plotSmooth(start, step, end, delta, func){
    var doc = app.activeDocument;
    var path = doc.pathItems.add();
    
    var index = 0;

    for (var t = start;  t <= end + delta; t += step) {
        var point = path.pathPoints.add();
        
        now =  func(t);
        
        third = step / (3 * 2 * delta);
        
        t1 = func(t + delta);
        t2 = func(t - delta);
        

        tan1 = new Array(now[0]+(t2[0] - t1[0]) * third, now[1]+(t2[1] - t1[1]) * third)
        tan2 = new Array(now[0]-(t2[0] - t1[0]) * third, now[1]-(t2[1] - t1[1]) * third)        
        
        point.anchor = now;
        point.leftDirection = tan1;
        point.rightDirection = tan2;
    }
    return path;
}