﻿#target illustrator

// jooHandlesToGeometry.jsx v1.0
//
// Copyright (c) 2015 Janne Ojala
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

try {
    app.activeDocument;
    main()
} catch(err) {
    alert("please open a document");    
}


function main(){
  var sym = createSymbolsIfNeeded(activeDocument);
  handlePaths(activeDocument.selection, 
              sym.anchor, sym.tangent); 
  // pass undefined for sym tangent if you dont want 
  // tangent handles.
}

function handlePaths(sel, symAcnhor, symDir) {
  for(var i = 0; i < sel.length; i++){
      if(sel[i].typename == "PathItem"){
          symbolsOnPoints(sel[i], symAcnhor, symDir);
      }
      // you would possibly need to recurse
      // the groups and compound paths in some 
      // cases.
  }
}

function symbolsOnPoints(path, symAcnhor, symDir) {
  var pts = path.pathPoints;
  for(var i = 0; i < pts.length; i++){
      var pos = pts[i].anchor
      
      if(!(symDir === undefined)) {
        var pos2 = pts[i].rightDirection;
        if (dist(pos, pos2) > 0.1){
          drawLine(pos, pos2);
          centeredSymbol(pos2, symDir);
        }
      
        pos2 = pts[i].leftDirection;
        if (dist(pos, pos2) > 0.1){
          drawLine(pos, pos2);
          centeredSymbol(pos2, symDir);
        }
      }
      
      centeredSymbol(pos, symAcnhor);
  }
}

function centeredSymbol(pos, symbol) {
  var p = pos.slice(0);
  sym = activeDocument.symbolItems.add(symbol);
  p[0] -= sym.width/2.0;
  p[1] += sym.height/2.0;   
  sym.position = p;
  return sym;
}

function drawLine(p1, p2) {
    var line = activeDocument.pathItems.add();
    line.setEntirePath([p1,p2]);
    return line;
}

function dist(a, b){

  return Math.sqrt(Math.pow(b[0]-a[0], 2) + 
                   Math.pow(b[1]-a[1], 2));        
}

function createSymbolsIfNeeded(doc){
    var symAnchor;
    try {
        symAnchor = doc.symbols.getByName("Anchor");
    }
    catch(err) {
        var circle = doc.pathItems.ellipse(5,5,5,5);
        symAnchor = doc.symbols.add(circle);
        symAnchor.name = "Anchor";
        circle.remove();
    }

    var symTangent;
    try {
        symTangent = doc.symbols.getByName("Tangent");
    }
    catch(err) {
        var rect = doc.pathItems.rectangle(5,5,5,5);
        var symTangent = doc.symbols.add(rect);
        symTangent.name = "Tangent";
        rect.remove();
    }
    return {anchor : symAnchor, tangent: symTangent};
}